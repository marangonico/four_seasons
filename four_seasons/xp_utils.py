from XPLMUtilities import *

from SandyBarbourUtilities import *


class XPLogger:

    LOG_LEVEL_INFO = 0
    LOG_LEVEL_DEBUG = 1

    log_level = None

    def __init__(self, log_level=LOG_LEVEL_INFO):
        self.log_level = log_level

    def set_level(self, log_level=LOG_LEVEL_INFO):
        self.log_level = log_level

    def log(self, msg='', value=None):

        if value is not None:
            log_msg = str(msg) + '=' + str(value) + '\n'
        else:
            log_msg = str(msg) + '\n'

        log_msg = "[Four Seasons]: " + log_msg

        XPLMDebugString(log_msg)

    def info(self, msg='', value=None):
        self.log(msg, value)

    def debug(self, msg='', value=None):
        if self.log_level == self.LOG_LEVEL_DEBUG:
            self.log(msg, value)

    def error(self, msg='', value=None):
        self.log(msg, value)
