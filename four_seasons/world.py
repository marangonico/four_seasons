from math import radians, sqrt, sin, cos, atan2, asin, degrees, pi

from XPLMGraphics import *
from XPLMScenery import *

from four_seasons import logger


__EARTH_RADIUS__ = 6372.8
__EPSILON__ = 0.000001      # threshold for floating-point equality


class World:

    DEFAULT_PROBE_DISTANCE = 20
    DEFAULT_PROBES_COUNT = 5

    probe = None

    default_scan_points = []

    def __init__(self):
        self.probe = XPLMCreateProbe(xplm_ProbeY)

        probe_degrees = range(0, 360, 30)

        self.default_scan_points = [(0, 0), ]
        for probe_degree in probe_degrees:
            for distance in range(
                    self.DEFAULT_PROBE_DISTANCE, self.DEFAULT_PROBE_DISTANCE * (self.DEFAULT_PROBES_COUNT + 1),
                    self.DEFAULT_PROBE_DISTANCE):

                self.default_scan_points.append((probe_degree, distance))

    def geocalc(self, lat1, lon1, lat2, lon2):

        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)
        dlon = lon1 - lon2
        y = sqrt((cos(lat2) * sin(dlon)) ** 2 + (cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dlon)) ** 2)
        x = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)
        c = atan2(y, x)
        return __EARTH_RADIUS__ * c

    def get_latlon_from_bearingdistance(self, lat1, lon1, bearing, distance):

        """
        Return final coordinates (lat2,lon2) [in degrees] given initial coordinates
        (lat1,lon1) [in degrees] and a bearing [in degrees] and distance [in km]
        """
        rlat1 = radians(lat1)
        rlon1 = radians(lon1)
        rbearing = radians(bearing)
        rdistance = distance / __EARTH_RADIUS__  # normalize linear distance to radian angle

        rlat = asin(sin(rlat1) * cos(rdistance) + cos(rlat1) * sin(rdistance) * cos(rbearing))

        if cos(rlat) == 0 or abs(cos(rlat)) < __EPSILON__:  # Endpoint a pole
            rlon = rlon1
        else:
            rlon = ((rlon1 - asin(sin(rbearing) * sin(rdistance) / cos(rlat)) + pi) % (2 * pi)) - pi

        lat = degrees(rlat)
        lon = degrees(rlon)

        return lat, lon

    def get_mean_elevation(self, lat, lon, scan_points=None):
        # https://forums.x-plane.org/index.php?/forums/topic/38688-how-do-i-use-xplmprobeterrainxyz/&page=2
        # logger.debug('get_mean_elevation from lat,lon', (lat, lon))

        if not scan_points:
            scan_points = self.default_scan_points

        # logger.debug('scan_points', scan_points)
        mean_elevation = 0
        for bearing, distance in scan_points:

            probe_lat, probe_lon = self.get_latlon_from_bearingdistance(lat, lon, bearing, distance)

            x, y, z = XPLMWorldToLocal(probe_lat, probe_lon, 0)

            info = []
            XPLMProbeTerrainXYZ(self.probe, x, y, z, info)

            _, __, probe_alt = XPLMLocalToWorld(info[1], info[2], info[3])
            x, y, z = XPLMWorldToLocal(probe_lat, probe_lon, probe_alt)

            info = []
            XPLMProbeTerrainXYZ(self.probe, x, y, z, info)
            _, __, probe_alt = XPLMLocalToWorld(info[1], info[2], info[3])

            mean_elevation += probe_alt
            # logger.debug('probe lat,lon,elevation', (probe_lat, probe_lon, probe_alt))

        mean_elevation = mean_elevation / len(scan_points)

        logger.debug('(lat, lon, mean_elevation)', (lat, lon, mean_elevation))
        return mean_elevation
