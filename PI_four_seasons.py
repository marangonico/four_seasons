import os
import ConfigParser

from XPLMDataAccess import *
from XPLMDefs import *
from XPLMDisplay import *
from XPLMGraphics import *
from XPLMMenus import *
from XPLMPlugin import *
from XPLMProcessing import *
from XPLMUtilities import *
from XPStandardWidgets import *
from XPWidgetDefs import *
from XPWidgets import *

from SandyBarbourUtilities import *

from four_seasons import __VERSION__, logger
from four_seasons.globals import *
from four_seasons.xp_ui import *
from four_seasons.XPDref import XPCustomDRefsMgr, XPDref
from four_seasons.zones import ZoneTemperateNorth, ZoneTropical, ZoneTemperateSouth, ZonePolar, ZoneTemperatePatagonia
from four_seasons.world import World

SEASON_DISABLED_DICT = {'value': SEASON_DISABLED, 'name': 'Disabled', 'help_text': '', 'slug': 'disabled'}


class FourSeasons:

    seasons = [
        SEASON_DISABLED_DICT,
        {'value': SEASON_SPRING, 'name': 'Spring', 'help_text': '', 'slug': 'spring'},
        {'value': SEASON_SUMMER, 'name': 'Summer', 'help_text': '', 'slug': 'summer'},
        {'value': SEASON_FALL, 'name': 'Fall', 'help_text': '', 'slug': 'fall'},
        {'value': SEASON_WINTER, 'name': 'Winter', 'help_text': '', 'slug': 'winter'},
        {'value': SEASON_WINTER_PATCHY_SNOW, 'name': 'Winter with Patchy Snow',
         'help_text': 'if Winter and temp. <= 0 degrees C', 'slug': 'winter_patchy_snow'},
        {'value': SEASON_WINTER_SNOW, 'name': 'Winter with Snow',
         'help_text': 'if Winter and temp. < -5 degrees C', 'slug': 'winter_with_snow'},
        {'value': SEASON_WINTER_DEEP, 'name': 'Deep Winter',
         'help_text': 'if Winter and temp. < -10 degrees C. Only used by TerraMaxx', 'slug': 'winter_deep'},
    ]

    automatic_modes = [
        {'value': AUTOMATIC_MODE_RELOAD_ALWAYS, 'name': 'Always', 'help_text': '', 'slug': 'always'},
        {'value': AUTOMATIC_MODE_RELOAD_ONGROUND, 'name': 'Only with aircraft on ground and stopped',
         'help_text': '', 'slug': 'onground'},
    ]

    ZONES = [
        ZoneTemperateNorth('temperate North Hemisphere', 26, -180, 90, 180),
        ZoneTropical('tropical', -25, -180, 25, 180),
        ZoneTemperatePatagonia('Patagonia', -62, -100, -41, -20),
        ZoneTemperateSouth('temperate South Hemisphere', -62, -180, -26, 180),
        # ZonePolar('north polar', 61, -180, 90, 180),
        ZonePolar('antartica', -90, -180, -61, 180),
    ]

    automatic_mode = None

    season = SEASON_SUMMER
    season_old = None
    season_expected = None

    local_date_days_dref = None
    local_date_days_last_value = None

    latitude_dref = None
    latitude_last_value = None

    temperature_sealevel_c_dref = None

    mean_elevation_meters = None
    last_mean_elevation_update_lat = None
    last_mean_elevation_update_lon = None

    season_change_temperature = -273.15     # let's start from 0 deg Kelvin
    season_change_time_sec = 0

    longitude_dref = None

    parent = None

    force_refresh = False
    force_refresh_if_summer = False
    force_refresh_if_winter_patchy_snow = False

    config_filename = os.path.join(os.path.dirname(__file__), 'PI_four_seasons.cfg')
    # logger.debug('config_filename', config_filename)
    config = None

    first_load_done = False

    pending_reload_status = False
    pending_reload_season_old = None

    xplane_version = None

    def __init__(self, parent):

        logger.info('init Four Seasons v{}'.format(__VERSION__))

        self.parent = parent

        self.xplane_version, XPLMVersion, HostID = XPLMGetVersions()

        self.world = World()

        self.latitude_dref = XPDref('sim/flightmodel/position/latitude', 'double')
        self.longitude_dref = XPDref('sim/flightmodel/position/longitude', 'double')
        self.position_elevation_msl_dref = XPDref("sim/flightmodel/position/elevation", "double")
        self.position_y_agl_meters_dref = XPDref("sim/flightmodel/position/y_agl", 'float')
        self.acf_is_onground_dref = XPDref("sim/flightmodel/failures/onground_any", 'int')
        self.position_groundspeed_dref = XPDref("sim/flightmodel/position/groundspeed", 'float')

        self.temperature_sealevel_c_dref = XPDref("sim/weather/temperature_sealevel_c", 'float')
        self.temperature_ambient_c_dref = XPDref("sim/weather/temperature_ambient_c", 'float')

        self.view_pitch_dref = XPDref("sim/graphics/view/view_pitch", 'float')
        self.view_is_external_dref = XPDref("sim/graphics/view/view_is_external", 'int')

        self.local_date_days_dref = XPDref("sim/time/local_date_days", 'int')
        self.total_running_time_sec_dref = XPDref('sim/time/total_running_time_sec', 'float')
        self.sim_is_paused_dref = XPDref('sim/time/paused', 'int')

        self.zulu_time_hours_dref = XPDref("sim/cockpit2/clock_timer/zulu_time_hours", "int")
        self.zulu_time_minutes_dref = XPDref("sim/cockpit2/clock_timer/zulu_time_minutes", "int")

        self.local_date_days_last_value = self.local_date_days_dref.value

        self.config = ConfigParser.ConfigParser()

        if not os.path.exists(self.config_filename):

            self.config.add_section('mode')
            self.config.set('mode', 'automatic_season', '1')
            self.config.set('mode', 'season', '')
            self.config.set('mode', 'treat_patchy_snow_as_snow', '0')
            self.config.set('mode', 'pause_sim', '0')

            self.config.add_section('terramaxx')
            self.config.set('terramaxx', 'enabled', '0')

            self.config.add_section('sam_seasons')
            self.config.set('sam_seasons', 'enabled', '0')

            self.config.add_section('debug')
            self.config.set('debug', 'level', '0')
            self.write_config()

        self.config.read(self.config_filename)

        try:
            terramaxx_enabled = self.config.get('terramax', 'enabled')
            self.config.remove_section('terramax')
            self.config.add_section('terramaxx')
            self.config.set('terramaxx', 'enabled', terramaxx_enabled)
            self.write_config()
        except ConfigParser.NoSectionError:
            pass

        try:
            _ = int(self.config.get('sam_seasons', 'enabled'))
        except ConfigParser.NoSectionError:
            self.config.add_section('sam_seasons')
            self.config.set('sam_seasons', 'enabled', '0')
            self.write_config()

        try:
            _ = int(self.config.get('mode', 'summer_off'))
        except ConfigParser.NoOptionError:
            self.config.set('mode', 'summer_off', '0')
            self.write_config()

        try:
            _ = int(self.config.get('mode', 'treat_patchy_snow_as_snow'))
        except ConfigParser.NoOptionError:
            self.config.set('mode', 'treat_patchy_snow_as_snow', '0')
            self.write_config()

        try:
            _ = int(self.config.get('mode', 'pause_sim'))
        except ConfigParser.NoOptionError:
            self.config.set('mode', 'pause_sim', '0')
            self.write_config()

        try:
            log_level = int(self.config.get('debug', 'level'))
        except ConfigParser.NoSectionError:
            self.config.add_section('debug')
            self.config.set('debug', 'level', '0')
            self.write_config()
            log_level = logger.LOG_LEVEL_INFO

        logger.set_level(log_level)

        logger.debug('config automatic_season', self.config.get('mode', 'automatic_season'))

        if self.config.get('terramaxx', 'enabled') == '1':
            self.parent.check_terramaxx_drefs()

        if self.config.get('sam_seasons', 'enabled') == '1':
            self.parent.check_sam_seasons_drefs()

    def reset(self):
        self.season_change_temperature = -273.15
        self.season_change_time_sec = 0

    def first_load(self):

        logger.debug('first load')
        lat = self.latitude_dref.value
        lon = self.longitude_dref.value

        if lat is None or lon is None:
            logger.debug('lat and lon are None')
            return

        self.first_load_done = True

        if self.config.get('mode', 'automatic_season'):
            self.calculate_season(set_season=True)
        else:
            logger.debug('manual season from config', int(self.config.get('mode', 'season')))
            self.set_season(int(self.config.get('mode', 'season')), first_load=True)

    def write_config(self):
        self.config.write(open(self.config_filename, 'w'))

    def get_season_from_value(self, season_value):
        try:
            return [season for season in self.seasons if season['value'] == season_value][0]
        except IndexError as e:
            return SEASON_DISABLED_DICT

    def get_season(self):
        return self.season

    def set_season(self, new_season=None, first_load=False):

        if not new_season:
            return

        if (new_season == SEASON_WINTER_PATCHY_SNOW and
                self.config.get('mode', 'treat_patchy_snow_as_snow') == '1'):
            logger.debug('treating "winter patchy snow" as "winter with snow"', new_season)
            new_season = SEASON_WINTER_SNOW

        if self.config.get('mode', 'automatic_season'):
            # this is necessary when switching from manual to automatic mode
            self.pending_reload_season_old = None

        self.season_old = self.season
        self.season = new_season
        logger.debug('set_season(old={}, new={})'.format(self.season_old, self.season))

        if self.season == SEASON_SUMMER and self.config.get('mode', 'summer_off') == '1':
            # use lower level summer textures
            logger.debug('summer_off=1, using lower level summer textures')
            self.parent.custom_drefs.drefs[DREF_SIG_SEASON].value = SEASON_DISABLED
        else:
            self.parent.custom_drefs.drefs[DREF_SIG_SEASON].value = self.season

        if self.config.get('terramaxx', 'enabled') == '1':
            self.parent.set_terramaxx_values(season=self.season)

        if self.config.get('sam_seasons', 'enabled') == '1':
            self.parent.set_sam_seasons_values(season=self.season)

        if (self.season_old != self.season or
                self.force_refresh or
                (self.force_refresh_if_summer and self.season == SEASON_SUMMER) or
                (self.force_refresh_if_winter_patchy_snow and self.season == SEASON_WINTER_PATCHY_SNOW)):

            if not self.mean_elevation_meters:
                self.update_mean_elevation()

            self.season_change_temperature = self.get_temperature()
            self.season_change_time_sec = self.total_running_time_sec_dref.value

            if self.force_refresh:
                logger.info('reloading scenery because force refresh = True')
            elif self.force_refresh_if_summer and self.season == SEASON_SUMMER:
                logger.info('reloading scenery because season is "summer" and summer_off has changed')
            elif self.force_refresh_if_winter_patchy_snow and self.season == SEASON_WINTER_PATCHY_SNOW:
                logger.info(
                    'reloading scenery because season is "winter with patchy snow" and '
                    'treat_patchy_snow_as_snow has changed'
                )
            else:
                if self.season == SEASON_DISABLED and self.season_old in (None, SEASON_DISABLED):
                    return

                if self.config.get('mode', 'automatic_season'):

                    if self.season != self.pending_reload_season_old:
                        logger.debug('pending reload status = True')
                        self.pending_reload_status = True
                        self.parent.show_season_change_window()
                        return
                    else:
                        logger.debug('pending reload already dismissed')
                        return

                elif first_load:

                    if self.season != SEASON_DISABLED:
                        # manual season during first load, then ask for confirmation (only if disabled)
                        logger.debug('pending reload status = True')
                        self.pending_reload_status = True
                        self.parent.show_season_change_window()

                    return

                logger.info('reloading scenery because season has changed from {} to {}'.format(
                    self.season_old, self.season))

            self.force_refresh = False
            self.force_refresh_if_summer = False
            self.force_refresh_if_winter_patchy_snow = False
            self.reload_textures()

        else:

            if self.season == self.season_old:
                logger.debug('not reloading because season has not changed')
            else:
                logger.debug('not reloading because refresh is not forced')

    def reload_textures(self):
        if self.xplane_version >= 11000:
            XPLMCommandOnce(XPLMFindCommand("sim/operation/reload_scenery"))
        else:
            XPLMReloadScenery()

    def get_temperature(self):
        return self.temperature_sealevel_c_dref.value - self.mean_elevation_meters / 1000 * 6.5

    def update_mean_elevation(self):

        if (self.latitude_dref.value != self.last_mean_elevation_update_lat or
                self.longitude_dref.value != self.last_mean_elevation_update_lon):

            self.last_mean_elevation_update_lat = self.latitude_dref.value
            self.last_mean_elevation_update_lon = self.longitude_dref.value

            self.mean_elevation_meters = self.world.get_mean_elevation(
                self.latitude_dref.value, self.longitude_dref.value)

    def calculate_season(self, set_season=True):

        logger.debug('** calculating season **')

        lat = self.latitude_dref.value
        lon = self.longitude_dref.value

        logger.debug(
            'lat={}, lon={}, doy={}, hh:mm={}:{}'.format(
                lat, lon, self.local_date_days_dref.value,
                self.zulu_time_hours_dref.value, self.zulu_time_minutes_dref.value
            )
        )

        if lat is None or lon is None:
            logger.debug('lat and lon are None')
            return None

        self.update_mean_elevation()

        zone_found = False
        for zone in self.ZONES:

            if zone.is_within(lat, lon):
                zone_found = True
                logger.debug('within zone {}'.format(zone.name))

                season = zone.get_season(
                    doy=self.local_date_days_dref.value,
                    lat=lat,
                    lon=lon,
                    temperature=self.get_temperature(),
                    elevation_msl=self.mean_elevation_meters,
                )
                logger.debug('zone.get_season', season)
                if (season == SEASON_WINTER_PATCHY_SNOW and
                        self.config.get('mode', 'treat_patchy_snow_as_snow') == '1'):
                    logger.debug('treating "winter patchy snow" as "winter with snow"', season)
                    season = SEASON_WINTER_SNOW
                break

        if not zone_found:
            logger.debug('No appliable zone')
            return None

        self.season_expected = season

        if set_season:
            self.set_season(season)

        self.force_refresh_if_summer = False
        self.force_refresh_if_winter_patchy_snow = False

        return season

    def is_automatic_mode_available(self):
        if self.config.get('mode', 'automatic_season') == '2':
            # is acf on ground and (almost) stopped?
            return self.acf_is_onground_dref.value == 1 and self.position_groundspeed_dref.value < 1
        elif self.config.get('mode', 'automatic_season') == '1':
            # always
            return True
        else:
            # invalid value
            return False

    def is_check_season_necessary(self):

        check_season = False

        if not self.local_date_days_last_value or self.local_date_days_dref.value != self.local_date_days_last_value:
            logger.debug('date has changed. season check is necessary')
            self.local_date_days_last_value = self.local_date_days_dref.value
            check_season = True

        return check_season

    def dismiss_pending_reload(self):

        self.pending_reload_season_old = self.season
        self.pending_reload_status = False


class PythonInterface:

    four_seasons = None

    custom_drefs = None

    flightmodel_loop_CB = None

    menu = None
    menu_cb = None
    menu_plugins_item = None
    menu_ref = None
    menu_item_control_panel = None

    window_handler_cb = None
    window_widget = None

    ui = None

    stopped = False

    refresh_seconds_onground = 10
    refresh_seconds_flying = 60

    delay_period_max_sec = 60 * 20
    delay_period_max_degrees_tollerance = 5

    first_load_delay_seconds = 10
    first_load_delayed = False

    sim_paused_on_season_change_request = False

    plugin_betterpushback_is_installed = False

    sam_seasons_drefs_handled_by_four_seasons = False

    def XPluginStart(self):

        SandyBarbourClearDisplay()

        self.Name = "Four Seasons"
        self.Sig = "nm.four_seasons"
        self.Desc = "A plugin driving season changes"

        self.custom_drefs = XPCustomDRefsMgr(self)
        self.custom_drefs.create_dref(DREF_SIG_SEASON, 'int')

        self.four_seasons = FourSeasons(self)

        self.ui = XPUI(python_interface=self)
        self.ui_season_change = XPUI(python_interface=self)

        # Main menu
        self.menu_plugins_item = XPLMAppendMenuItem(XPLMFindPluginsMenu(), 'Four Seasons', 0, 1)
        self.menu_cb = self.menu_callback
        self.menu_ref = XPLMCreateMenu(
            self, 'Four Seasons', XPLMFindPluginsMenu(), self.menu_plugins_item, self.menu_cb, 0)
        self.menu_item_control_panel = XPLMAppendMenuItem(self.menu_ref, 'Control Panel', 100, 1)

        self.flightmodel_loop_CB = self.flightmodel_loop_callback
        XPLMRegisterFlightLoopCallback(self, self.flightmodel_loop_CB, 1.0, 0)

        plugin_id = XPLMFindPluginBySignature("skiselkov.BetterPushback")
        if plugin_id != XPLM_NO_PLUGIN_ID:
            self.plugin_betterpushback_is_installed = True

        return self.Name, self.Sig, self.Desc

    def get_refresh_seconds(self):

        if self.four_seasons.acf_is_onground_dref.value == 1:
            return self.refresh_seconds_onground
        else:
            return self.refresh_seconds_flying

    def flightmodel_loop_callback(self, elapsedMe, elapsedSim, counter, refcon):

        if self.stopped:
            return 0

        if (self.plugin_betterpushback_is_installed and
                self.four_seasons.view_is_external_dref.value == 1 and
                self.four_seasons.view_pitch_dref.value == -90):
            # betterpushback pre-plan phase
            logger.debug('skipping betterpushback pre-plan phase')
            return self.update_control_panel_and_wait(self.get_refresh_seconds())

        if not self.four_seasons.first_load_done:

            if not self.first_load_delayed:
                logger.debug('first load delay period')
                self.first_load_delayed = True
                return self.first_load_delay_seconds
            else:
                self.four_seasons.first_load()

        if self.four_seasons.pending_reload_status:
            # waiting for user confirmation
            return self.get_refresh_seconds()

        if self.four_seasons.config.get('mode', 'automatic_season'):
            self.four_seasons.update_mean_elevation()
            temperature_current = self.four_seasons.get_temperature()
            temperature_difference = abs(temperature_current - self.four_seasons.season_change_temperature)

            logger.debug(
                '(season_change_temperature, temperature_current, temperature_difference)',
                (self.four_seasons.season_change_temperature, temperature_current, temperature_difference)
            )

            if temperature_difference > self.delay_period_max_degrees_tollerance:
                delay_period_end_sec = 0
            else:
                delay_period_duration_sec = self.delay_period_max_sec - (
                    self.delay_period_max_sec * temperature_difference / self.delay_period_max_degrees_tollerance)
                delay_period_end_sec = self.four_seasons.season_change_time_sec + delay_period_duration_sec
                logger.debug(
                    '(delay period duration minutes, remaining)',
                    (
                        delay_period_duration_sec / 60,
                        (delay_period_end_sec - self.four_seasons.total_running_time_sec_dref.value) / 60
                    ))
        else:
            delay_period_end_sec = 0

        if delay_period_end_sec > 0:

            if self.four_seasons.is_check_season_necessary():
                # interrupt delay period and start over calculating the season
                pass
            else:
                if self.four_seasons.total_running_time_sec_dref.value < delay_period_end_sec:
                    # acf is flying and delay period is underway
                    logger.debug('within delay period')
                    return self.update_control_panel_and_wait(self.get_refresh_seconds())

        # calculate expected season
        self.four_seasons.calculate_season(set_season=False)

        self.refresh_control_panel()

        plugin_id = XPLMFindPluginBySignature("xplanesdk.examples.DataRefEditor")
        if plugin_id != XPLM_NO_PLUGIN_ID:
            self.custom_drefs.notify_datarefeditor(plugin_id)

        if self.four_seasons.config.get('mode', 'automatic_season'):
            if self.four_seasons.is_automatic_mode_available():
                self.four_seasons.set_season(self.four_seasons.season_expected)

        return self.get_refresh_seconds()

    def XPluginStop(self):

        self.stopped = True

        self.custom_drefs.destroy_all()

        if self.flightmodel_loop_CB:
            XPLMUnregisterFlightLoopCallback(self, self.flightmodel_loop_CB, 0)

        if self.ui.exists('wnd_control_panel'):
            self.ui.destroy_widget('wnd_control_panel')

        if self.menu_plugins_item:
            XPLMRemoveMenuItem(XPLMFindPluginsMenu(), self.menu_plugins_item)

        if self.menu_ref:
            XPLMDestroyMenu(self, self.menu_ref)

    def XPluginEnable(self):
        return 1

    def XPluginDisable(self):
        """"""

    def XPluginReceiveMessage(self, inFromWho, inMessage, inParam):

        if inMessage:

            if self.stopped or self.four_seasons.first_load_done is False:
                return

            if inMessage == XPLM_MSG_AIRPORT_LOADED:

                logger.debug('XPLM_MSG_AIRPORT_LOADED')
                if self.four_seasons is None:
                    self.four_seasons = FourSeasons(self)

                self.four_seasons.reset()

                # calculate expected season
                self.four_seasons.calculate_season(set_season=False)

                self.refresh_control_panel()

                if self.four_seasons.config.get('mode', 'automatic_season'):
                    if self.four_seasons.is_automatic_mode_available():
                        self.four_seasons.set_season(self.four_seasons.season_expected)

    def menu_callback(self, menuRef, menu_item):

        if menu_item:
            menu_item_string = str(menu_item)
            # logger.debug(menu_item_string)
            if menu_item_string == '100':

                if not self.ui.exists('wnd_control_panel'):
                    self.create_control_panel_window()
                    self.ui.widgets['wnd_control_panel'].show()
                else:
                    if not self.ui.widgets['wnd_control_panel'].is_visible():
                        self.ui.widgets['wnd_control_panel'].show()
                        self.refresh_control_panel()

    def create_control_panel_window(self):

        control_panel_window = self.ui.create_widget(
            XPWidgetWindow,
            name='wnd_control_panel',
            descriptor='.: Four Seasons (v{}) by Nicola Marangon :.'.format(__VERSION__),
            x1=20, y1=650, w=570, h=520, pad_x=20, pad_y=15,
            is_root=True,
            has_close_boxes=True,
            childs_pad_x=1, childs_pad_y=1,
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_status',
            descriptor='',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.refresh_control_panel()

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_automatic_season',
            descriptor='Automatically determine the season and reload textures as follows:',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        control_panel_window.pad_x = control_panel_window.pad_x * 2
        control_panel_window.filler.reset_x()
        # logger.debug(self.four_seasons.config.get('mode', 'automatic_season'))
        # automatic mode buttons
        for item_idx, automatic_mode in enumerate(self.four_seasons.automatic_modes):
            # logger.debug('btn_automatic_mode_{}'.format(automatic_mode['slug']))
            # logger.debug(automatic_mode['value'])
            # logger.debug(self.four_seasons.config.get('mode', 'automatic_season') == str(automatic_mode['value']))
            self.ui.create_widget(
                XPWidgetRadioButton,
                name='btn_automatic_mode_{}'.format(automatic_mode['slug']),
                tag=automatic_mode['name'],
                parent_container=control_panel_window,
                fill='x+',
                state=(1 if self.four_seasons.config.get('mode', 'automatic_season') == str(automatic_mode['value']) else 0),
                group='automatic_modes',
            )

            self.ui.create_widget(
                XPWidgetLabel,
                name='lbl_automatic_mode_{}'.format(automatic_mode['slug']),
                descriptor='{} {}'.format(
                    automatic_mode['name'],
                    '(' + automatic_mode['help_text'] + ')' if automatic_mode['help_text'] else ''
                ),
                parent_container=control_panel_window,
                fill='y+,x0',
            )

        control_panel_window.pad_x = control_panel_window.pad_x / 2
        control_panel_window.filler.reset_x()

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_manual_selections', descriptor='Manual mode selections:',
            parent_container=control_panel_window,
            pad_y=0,
            fill='y/2+,x0',
        )

        control_panel_window.pad_x = control_panel_window.pad_x * 2
        control_panel_window.filler.reset_x()

        # seasons buttons
        for item_idx, season in enumerate(self.four_seasons.seasons):
            # logger.debug('btn_manual_season_{}'.format(season['name'].lower()))
            self.ui.create_widget(
                XPWidgetRadioButton,
                name='btn_manual_season_{}'.format(season['slug']),
                tag=season['name'],
                parent_container=control_panel_window,
                fill='x+',
                state=(1 if not self.four_seasons.config.get('mode', 'automatic_season') and
                            self.four_seasons.get_season() == season['value'] else 0),
                group='seasons_checks',
            )

            self.ui.create_widget(
                XPWidgetLabel,
                name='lbl_manual_season_{}'.format(season['slug']),
                descriptor='{} {}'.format(
                    season['name'], '(' + season['help_text'] + ')' if season['help_text'] else ''),
                parent_container=control_panel_window,
                fill='y+,x0',
            )

        control_panel_window.pad_x = control_panel_window.pad_x / 2
        control_panel_window.filler.reset_x()

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_options',
            descriptor='Options:',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetCheckButton,
            name='btn_treat_patchy_snow_as_snow',
            parent_container=control_panel_window,
            fill='x+',
            state=1 if self.four_seasons.config.get(
                'mode', 'treat_patchy_snow_as_snow') == '1' else 0,
            group='treat_patchy_snow_as_snow',
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_treat_patchy_snow_as_snow',
            descriptor='Treat "Winter with Patchy Snow" as "Winter with Snow"',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetCheckButton,
            name='btn_summer_off',
            parent_container=control_panel_window,
            fill='x+',
            state=1 if self.four_seasons.config.get('mode', 'summer_off') == '1' else 0,
            group='summer_off',
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_summer_off',
            descriptor='When summer, use non seasonal/default textures (e.g. SFD Global)',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetCheckButton,
            name='btn_terramaxx',
            parent_container=control_panel_window,
            fill='x+',
            state=1 if self.four_seasons.config.get('terramaxx', 'enabled') == '1' else 0,
            group='terramaxx',
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_terramaxx',
            descriptor='Feed TerraMaxx datarefs',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetCheckButton,
            name='btn_sam_seasons',
            parent_container=control_panel_window,
            fill='x+',
            state=1 if self.four_seasons.config.get('sam_seasons', 'enabled') == '1' else 0,
            group='third_parties',
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_sam_seasons',
            descriptor='Feed SAM Seasons datarefs (Needs a simulator restart if not enabled at startup)',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetCheckButton,
            name='btn_pause_sim',
            parent_container=control_panel_window,
            fill='x+',
            state=1 if self.four_seasons.config.get('mode', 'pause_sim') == '1' else 0,
            group='pause_sim',
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_pause_sim',
            descriptor='Pause the simulator on season change',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetCheckButton,
            name='btn_debug_level',
            parent_container=control_panel_window,
            fill='x+',
            state=1 if self.four_seasons.config.get('debug', 'level') == '1' else 0,
            group='debug',
        )

        self.ui.create_widget(
            XPWidgetLabel,
            name='lbl_debug',
            descriptor='Write debug info to log.txt',
            parent_container=control_panel_window,
            fill='y+,x0',
        )

        self.ui.create_widget(
            XPWidgetPushButton,
            name='btn_help',
            descriptor='Help',
            parent_container=control_panel_window,
            x1=0,
        )

        self.ui.create_widget(
            XPWidgetPushButton,
            name='btn_refresh_and_reload',
            descriptor='Refresh & Reload',
            parent_container=control_panel_window,
            x1=180,
        )

        self.ui.create_widget(
            XPWidgetPushButton,
            name='btn_calculate_season',
            descriptor='Calculate Season',
            parent_container=control_panel_window,
            x1=300,
        )

        self.ui.create_widget(
            XPWidgetPushButton,
            name='btn_apply',
            descriptor='Save & Apply',
            parent_container=control_panel_window,
            x1=420,
        )

        # Register our widget handler
        self.ui.widgets['wnd_control_panel'].register_handler(
            python_interface=self, handler_function=self.control_panel_window_handler_callback)

    def refresh_control_panel(self):

        if self.ui.exists('wnd_control_panel') and self.ui.widgets['wnd_control_panel'].is_visible():

            if self.four_seasons.season_expected:
                season_expected_string = self.four_seasons.get_season_from_value(self.four_seasons.season_expected)['name']
                logger.debug('season_expected', (self.four_seasons.season_expected, season_expected_string))
            else:
                season_expected_string = ''

            self.four_seasons.update_mean_elevation()

            self.ui.widgets['lbl_status'].set_descriptor(
                'Current: {} - Expected: {} - Temp: {:5.2f} deg C'.format(
                    self.four_seasons.get_season_from_value(self.four_seasons.season)['name'],
                    season_expected_string,
                    self.four_seasons.get_temperature(),
                )
            )

    def update_control_panel_and_wait(self, refresh_seconds):
        self.refresh_control_panel()
        return refresh_seconds

    def control_panel_window_handler_callback(self, inMessage, inWidget, inParam1, inParam2):

        if not (self.ui.exists('wnd_control_panel') and self.ui.widgets['wnd_control_panel'].is_visible()):
            return 1

        if inMessage == xpMessage_CloseButtonPushed:

            if self.ui.exists('wnd_control_panel'):
                self.ui.widgets['wnd_control_panel'].hide()

            return 1

        # treat_patchy_snow_as_snow button
        if (inMessage == xpMsg_ButtonStateChanged and
                inParam1 == self.ui.widgets['btn_treat_patchy_snow_as_snow'].reference):

            treat_patchy_snow_as_snow = '1' if self.ui.widgets['btn_treat_patchy_snow_as_snow'].value else '0'
            logger.debug('treat_patchy_snow_as_snow', treat_patchy_snow_as_snow)
            self.four_seasons.force_refresh_if_winter_patchy_snow = (
                    self.four_seasons.config.get('mode', 'treat_patchy_snow_as_snow') != treat_patchy_snow_as_snow)

            self.four_seasons.config.set('mode', 'treat_patchy_snow_as_snow', treat_patchy_snow_as_snow)
            return 1

        # summer off button
        if inMessage == xpMsg_ButtonStateChanged and inParam1 == self.ui.widgets['btn_summer_off'].reference:

            summer_off = '1' if self.ui.widgets['btn_summer_off'].value else '0'
            self.four_seasons.force_refresh_if_summer = (
                    self.four_seasons.config.get('mode', 'summer_off') != summer_off)

            self.four_seasons.config.set('mode', 'summer_off', summer_off)
            return 1

        # terramaxx button
        if inMessage == xpMsg_ButtonStateChanged and inParam1 == self.ui.widgets['btn_terramaxx'].reference:
            if self.ui.widgets['btn_terramaxx'].value:
                self.four_seasons.config.set('terramaxx', 'enabled', '1')
                self.set_terramaxx_values(self.four_seasons.season)
            else:
                self.four_seasons.config.set('terramaxx', 'enabled', '0')
                self.reset_terramaxx_values()
                self.destroy_terramaxx_drefs()

            return 1

        # SAM Seasons button
        if inMessage == xpMsg_ButtonStateChanged and inParam1 == self.ui.widgets['btn_sam_seasons'].reference:
            if self.ui.widgets['btn_sam_seasons'].value:
                self.four_seasons.config.set('sam_seasons', 'enabled', '1')
                self.set_sam_seasons_values(self.four_seasons.season)
            else:
                self.four_seasons.config.set('sam_seasons', 'enabled', '0')
                self.reset_sam_seasons_values()
                self.destroy_sam_seasons_drefs()

            return 1

        # pause sim button
        if inMessage == xpMsg_ButtonStateChanged and inParam1 == self.ui.widgets['btn_pause_sim'].reference:

            pause_sim = '1' if self.ui.widgets['btn_pause_sim'].value else '0'
            self.four_seasons.config.set('mode', 'pause_sim', pause_sim)
            return 1

        # debug level button
        if inMessage == xpMsg_ButtonStateChanged and inParam1 == self.ui.widgets['btn_debug_level'].reference:
            if self.ui.widgets['btn_debug_level'].value:
                self.four_seasons.config.set('debug', 'level', '1')
                logger.set_level(logger.LOG_LEVEL_DEBUG)
            else:
                self.four_seasons.config.set('debug', 'level', '0')
                logger.set_level(logger.LOG_LEVEL_INFO)

            return 1

        # automatic mode radiobuttons
        radiobuttons = self.ui.get_widgets_from_groups(['automatic_modes', ])
        radiobutton_references = [button.reference for button in radiobuttons]
        if inMessage == xpMsg_ButtonStateChanged and inParam1 in radiobutton_references:

            for reference in radiobutton_references:
                # logger.debug('reference', reference)
                if reference != inParam1:
                    XPSetWidgetProperty(reference, xpProperty_ButtonState, 0)
                else:
                    automatic_mode_value = None
                    selected_button_name = [button.tag for button in radiobuttons if button.reference == reference][0]
                    if selected_button_name:
                        for automatic_mode in self.four_seasons.automatic_modes:
                            if automatic_mode['name'] == selected_button_name:
                                automatic_mode_value = automatic_mode['value']

                        self.four_seasons.config.set(
                            'mode', 'automatic_season', str(automatic_mode_value) if automatic_mode_value else '')

                        self.four_seasons.config.set('mode', 'season', '')

            # reset seasons radio buttons
            for seasons_radiobutton_reference in [
                seasons_button.reference for seasons_button in self.ui.get_widgets_from_groups(
                    ['seasons_checks', ])]:
                XPSetWidgetProperty(seasons_radiobutton_reference, xpProperty_ButtonState, 0)

            return 1

        # seasons checks radiobuttons
        radiobuttons = self.ui.get_widgets_from_groups(['seasons_checks', ])
        radiobutton_references = [button.reference for button in radiobuttons]
        if inMessage == xpMsg_ButtonStateChanged and inParam1 in radiobutton_references:

            for reference in radiobutton_references:
                if reference != inParam1:
                    XPSetWidgetProperty(reference, xpProperty_ButtonState, 0)
                else:
                    manual_season_value = None
                    selected_button_name = [button.tag for button in radiobuttons if button.reference == reference][0]
                    if selected_button_name:
                        for season in self.four_seasons.seasons:
                            if season['name'] == selected_button_name:
                                manual_season_value = season['value']

                        if manual_season_value is None:
                            manual_season_value = SEASON_SUMMER

                        logger.debug('manual_season_value', manual_season_value)
                        self.four_seasons.config.set('mode', 'season', str(manual_season_value))
                        self.four_seasons.config.set('mode', 'automatic_season', '')

                        # self.four_seasons.season = manual_season_value

            # reset automatic modes radio buttons
            for automatic_modes_radiobutton_reference in [
                    automatic_modes_button.reference for automatic_modes_button in self.ui.get_widgets_from_groups(
                        ['automatic_modes', ])]:

                XPSetWidgetProperty(automatic_modes_radiobutton_reference, xpProperty_ButtonState, 0)

            return 1

        if inMessage == xpMsg_PushButtonPressed:

            if inParam1 == self.ui.widgets['btn_help'].reference:

                import webbrowser
                webbrowser.open_new_tab('https://marangonico.gitbook.io/four-seasons/')
                return 1

            if inParam1 == self.ui.widgets['btn_refresh_and_reload'].reference:

                logger.debug('user pressed btn_refresh_and_reload')
                self.four_seasons.force_refresh = True
                self.dismiss_pending_reload()

                if self.four_seasons.config.get('mode', 'automatic_season'):
                    self.four_seasons.calculate_season()
                else:
                    self.four_seasons.set_season(self.four_seasons.season)

                self.four_seasons.force_refresh = False
                return 1

            if inParam1 == self.ui.widgets['btn_calculate_season'].reference:

                self.dismiss_pending_reload()
                if self.four_seasons.config.get('mode', 'automatic_season'):
                    self.four_seasons.calculate_season()
                else:
                    self.four_seasons.set_season(self.four_seasons.season)

                self.refresh_control_panel()

                return 1

            if inParam1 == self.ui.widgets['btn_apply'].reference:

                logger.debug('user pressed btn_apply')

                self.four_seasons.write_config()

                if self.four_seasons.config.get('mode', 'automatic_season'):
                    logger.debug('automatic_season', self.four_seasons.config.get('mode', 'automatic_season'))
                    self.four_seasons.calculate_season()
                else:
                    manual_season = int(self.four_seasons.config.get('mode', 'season'))

                    logger.debug('applying manual season', (
                        manual_season, self.four_seasons.get_season_from_value(manual_season)['name'])
                    )

                    self.four_seasons.set_season(manual_season)

                return 1

        return 0

    def create_season_change_window(self):

        season_change_window = self.ui_season_change.create_widget(
            XPWidgetWindow,
            name='wnd_season_change',
            descriptor='.: Four Seasons :. - Season change detected! -',
            x1=20, y1=780, w=500, h=120, pad_x=20, pad_y=15,
            is_root=True,
            has_close_boxes=True,
            childs_pad_x=1, childs_pad_y=1,
        )

        self.ui_season_change.create_widget(
            XPWidgetLabel,
            name='lbl_season_change_msg',
            descriptor='',
            parent_container=season_change_window,
            fill='y+,x0',
        )

        self.ui_season_change.create_widget(
            XPWidgetLabel,
            name='lbl_reload_msg',
            descriptor='Do you want to reload the textures now?',
            parent_container=season_change_window,
            fill='y+,x0',
        )

        self.ui_season_change.create_widget(
            XPWidgetPushButton,
            name='btn_yes',
            descriptor='Yes',
            parent_container=season_change_window,
            x1=5,
        )

        self.ui_season_change.create_widget(
            XPWidgetPushButton,
            name='btn_no',
            descriptor='No',
            parent_container=season_change_window,
            x1=350,
        )

        self.ui_season_change.widgets['wnd_season_change'].register_handler(
            python_interface=self, handler_function=self.season_change_window_handler_callback)

    def refresh_season_change_window(self):

        if self.four_seasons.config.get('mode', 'automatic_season'):
            self.ui_season_change.widgets['lbl_season_change_msg'].set_descriptor(
                'Season has changed from "{}" to "{}"'.format(
                    self.four_seasons.get_season_from_value(self.four_seasons.season_old)['name'],
                    self.four_seasons.get_season_from_value(self.four_seasons.season)['name'],
                )
            )
        else:
            self.ui_season_change.widgets['lbl_season_change_msg'].set_descriptor(
                'Four Seasons has been manually set for season "{}"'.format(
                    self.four_seasons.get_season_from_value(self.four_seasons.season)['name'],
                )
            )

    def resume_sim(self):

        if (self.four_seasons.config.get('mode', 'pause_sim') == '1' and
                not self.sim_paused_on_season_change_request and
                self.four_seasons.sim_is_paused_dref.value == 1):

            XPLMCommandOnce(XPLMFindCommand("sim/operation/pause_toggle"))

    def season_change_window_handler_callback(self, inMessage, inWidget, inParam1, inParam2):

        if not (self.ui_season_change.exists('wnd_season_change') and
                self.ui_season_change.widgets['wnd_season_change'].is_visible()):

            return 1

        if inMessage == xpMessage_CloseButtonPushed:

            if self.ui_season_change.exists('wnd_season_change'):
                logger.debug('user closed wnd_season_change')
                self.dismiss_pending_reload()
                self.resume_sim()

            return 1

        if inMessage == xpMsg_PushButtonPressed:

            if inParam1 == self.ui_season_change.widgets['btn_yes'].reference:
                logger.debug('user confirmed textures reload')
                self.dismiss_pending_reload()
                self.four_seasons.reload_textures()
                self.resume_sim()

                return 1

            if inParam1 == self.ui_season_change.widgets['btn_no'].reference:
                logger.debug('user declined textures reload')
                self.dismiss_pending_reload()
                self.resume_sim()

                return 1

        return 0

    def show_season_change_window(self):
        self.sim_paused_on_season_change_request = self.four_seasons.sim_is_paused_dref.value == 1

        if self.four_seasons.config.get('mode', 'pause_sim') == '1' and not self.sim_paused_on_season_change_request:
            XPLMCommandOnce(XPLMFindCommand("sim/operation/pause_toggle"))

        if not self.ui_season_change.exists('wnd_season_change'):
            self.create_season_change_window()
            self.refresh_season_change_window()
            self.ui_season_change.widgets['wnd_season_change'].show()
        else:
            if not self.ui_season_change.widgets['wnd_season_change'].is_visible():
                self.refresh_season_change_window()
                self.ui_season_change.widgets['wnd_season_change'].show()

    def dismiss_pending_reload(self):
        self.four_seasons.dismiss_pending_reload()
        if self.ui_season_change.exists('wnd_season_change'):
            self.ui_season_change.widgets['wnd_season_change'].hide()

    def check_terramaxx_drefs(self):

        if not self.custom_drefs.exists(DREF_SIG_TERRAMAX_IS_SUMMER):
            for signature in TERRAMAX_DREF_SIGNATURES:
                self.custom_drefs.create_dref(signature, 'int', writeable=True)

    def destroy_terramaxx_drefs(self):

        for signature in TERRAMAX_DREF_SIGNATURES:
            self.custom_drefs.destroy_dref(signature)

    def reset_terramaxx_values(self):

        for signature in TERRAMAX_DREF_SIGNATURES:
            self.custom_drefs.set_value(signature, 0)

    def set_terramaxx_values(self, season):

        self.check_terramaxx_drefs()
        self.reset_terramaxx_values()

        logger.debug('set_terramaxx_values for season=', season)

        if season in (SEASON_SPRING, SEASON_SUMMER,):
            if season == SEASON_SUMMER and self.four_seasons.config.get('mode', 'summer_off') == '1':
                self.custom_drefs.set_value(DREF_SIG_TERRAMAX_IS_SUMMER, 0)
            else:
                logger.debug('activated is_summer')
                self.custom_drefs.set_value(DREF_SIG_TERRAMAX_IS_SUMMER, 1)
        elif season in (SEASON_FALL, SEASON_WINTER,):
            logger.debug('activated is_autumn')
            self.custom_drefs.set_value(DREF_SIG_TERRAMAX_IS_AUTUMN, 1)
        elif season in (SEASON_WINTER_PATCHY_SNOW, SEASON_WINTER_SNOW,):
            logger.debug('activated is_mid_winter')
            self.custom_drefs.set_value(DREF_SIG_TERRAMAX_IS_MID_WINTER, 1)
        elif season == SEASON_WINTER_DEEP:
            logger.debug('activated is_winter')
            self.custom_drefs.set_value(DREF_SIG_TERRAMAX_IS_WINTER, 1)

    def check_sam_seasons_drefs(self):

        if not self.custom_drefs.exists(DREF_SIG_SAM_SEASONS_SUMMER):
            self.sam_seasons_drefs_handled_by_four_seasons = True
            for signature in SAM_SEASONS_DREF_SIGNATURES:
                self.custom_drefs.create_dref(signature, 'int')

    def destroy_sam_seasons_drefs(self):
        if self.sam_seasons_drefs_handled_by_four_seasons:
            for signature in SAM_SEASONS_DREF_SIGNATURES:
                self.custom_drefs.destroy_dref(signature)

    def reset_sam_seasons_values(self):
        for signature in SAM_SEASONS_DREF_SIGNATURES:
            self.custom_drefs.set_value(signature, 0)

    def set_sam_seasons_values(self, season):

        self.check_sam_seasons_drefs()
        self.reset_sam_seasons_values()

        logger.debug('set SAM seasons values for season=', season)

        if season in (SEASON_SPRING, ):
            logger.debug('activated spring')
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_SPRING, 1)
        if season in (SEASON_SUMMER,):
            logger.debug('activated is_summer')
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_SUMMER, 1)
        elif season in (SEASON_FALL,):
            logger.debug('activated is_autumn')
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_AUTUMN, 1)
        elif season in (SEASON_WINTER,):
            logger.debug('activated winter')
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_WINTER, 1)
        elif season in (SEASON_WINTER_PATCHY_SNOW, SEASON_WINTER_SNOW,):
            logger.debug('activated deepwinter')
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_DEEP_WINTER, 1)
        elif season == SEASON_WINTER_DEEP:
            logger.debug('activated deepwinter with snowytrees')
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_DEEP_WINTER, 1)
            self.custom_drefs.set_value(DREF_SIG_SAM_SEASONS_SNOWYTREES, 1)
